#include "CDriver_Bitflow.hpp"
#include <cstring>
#include <time.h> 
#include <sstream>
#include <iomanip>
#include <opencv2/opencv.hpp>

void LDBdisplay(char *format, ...)
{
	va_list	val;
	char	buff[8192];

	va_start(val,format);
	vsprintf(buff,format,val);
	va_end(val);
	printf("LDB: %s\n",buff);
	fflush(stdout);
}

void CDriver_Bitflow::show(char *format, ...)
{
	va_list	val;
	char	buff[8192];
	va_start(val, format);
	vsprintf(buff, format, val);
	va_end(val);
	std::printf("%s", buff);
	fflush(stdout);
}

void CDriver_Bitflow::finalize() {
	if (!initialized) return;
	
	//Stop grab image
	stop();
	
	initialized = false;
	
	//Reset all errors.
	tCIRC circ = CiAqSWreset(sCIp);
	if (kCIEnoErr != circ) {
		show("Err: CiAqSWreset (end) gave '%s'\n", CiErrStr(circ));
	}
	
	//Unmap the frame buffers.
	if ((nullptr != uPtrs) && (kCIEnoErr != CiUnmapFrameBuffers(sCIp))) {
		show("Err: CiUnmapFrameBuffers gave '%s'\n" , CiErrStr(circ));
	}
	
	//Release the DMA storage.
	 circ = CiDrvrBuffConfigure(sCIp, 0, 0, 0, 0, 0);
	if (circ != kCIEnoErr) {
		show("Err: CiDrvrBuffConfigure(2) gave '%s'\n",CiErrStr(circ));
	}
	
	// //Now set for circular-buffers.
	// circ = CiSetBufferMode(sCIp, kCIcircularBuffers);
	// if (kCIEnoErr != circ) {
	// 	show("Err: CiSetBufferMode (circular) gave '%s'\n", CiErrStr(circ));
	// 	return;
	//  }
	
	for (auto ptr:sPtrVec) { delete ptr; }

	//Close the access.
	if ((nullptr != sCIp) && (kCIEnoErr != CiVFGclose(sCIp))) {
		show("Err: CiVFGclose gave '%s'\n", CiErrStr(circ));
		return;
	}
}

void CDriver_Bitflow::initialize() {
	tCIRC circ;

#ifdef DEBUG
	// Set library debug function if '-D' was specified.
	circ = CiSetDebug(NULL, -1, LDBdisplay);
	if (circ != kCIEnoErr) {
		show("Err: CiSetDebug of %d gave '%s'\n", sNdx, CiErrStr(circ));
		return;
	}
#endif
	
	//Open the ndx'th frame grabber with exclusive write permission.
	circ = CiVFGopen(sNdx, kCIBO_exclusiveWrAccess, &sCIp);
	if (circ != kCIEnoErr) {
		show("Err: CiVFGopen gave '%s'\n", CiErrStr(circ));
		return;
	}
	
	//Init the VFG with the config file specified by argv (or DIP switches)
	circ = CiVFGinitialize(sCIp, (tCISTRZ)sCfgFN.c_str());
	if (circ != kCIEnoErr) {
		show("Err: CiVFGinitialize gave '%s'\n", CiErrStr(circ));
		finalize();
		return;
	}
	
	initialized = configureBuffer();
}

bool CDriver_Bitflow::configureBuffer() {	
	//Set the VFG for consume-buffers: start by releasing camfile buffers.
	tCIRC circ = CiDrvrBuffConfigure(sCIp, 0, 0, 0, 0, 0);
	if (kCIEnoErr != circ) {
		show("Err: CiDrvrBuffConfigure gave '%s'\n", CiErrStr(circ));
		finalize();
		return false;
	}
	
	// //Now set for consume-buffers.
	// circ = CiSetBufferMode(sCIp, kCIconsumeBuffers);
	// if (kCIEnoErr != circ) {
	// 	show("Err: CiSetBufferMode (consume) gave '%s'\n", CiErrStr(circ));
	// 	finalize();
	// 	return false;
	// }
	
	//Configure the VFG for 50 frame buffers.
	circ = CiDrvrBuffConfigure(sCIp, 50, 0, 0, 0, 0);
	if (kCIEnoErr != circ) {
		show("Err: CiDrvrBuffConfigure gave '%s'\n", CiErrStr(circ));
		finalize();
		return false;
	}
	
	//Determine buffer configuration.  We only need bitsPerPix and stride.
	tCIU32 nFrames = 0;
	circ = CiBufferInterrogate(
				sCIp,
				&nFrames,
				&bitsPerPix,
				&hROIoffset,
				&hROIsize,
				&vROIoffset,
				&vROIsize,
				&stride);
	if (kCIEnoErr != circ) {
		show("Err: CiBufferInterrogate gave '%s'\n", CiErrStr(circ));
		finalize();
		return false;
	}
	
	//Release all buffers.
	for (int i=0; i<nFrames; i++) { CiReleaseBuffer(sCIp, i); }
	
	
	//	Get the buffer pointers for read/write access to buffers.
	//	We ask for write access because we are going to clear the buffer
	//	lines after they are displayed.

	circ = CiMapFrameBuffers(sCIp, 0, &nPtrs, &uPtrs);
	if (kCIEnoErr != circ) {
		show("Err: CiMapFrameBuffers gave '%s'\n", CiErrStr(circ));
		finalize();
		return false;
	}
	
	return true;
}

void CDriver_Bitflow::start() {
	if (!initialized) return;
	
	stop();
	
	//Reset acquisition and clear all error conditions.
	tCIRC circ = CiAqSWreset(sCIp);
	if (kCIEnoErr != circ) {
		show("Err: CiAqSWreset gave '%s'\n", CiErrStr(circ));
		finalize();
		return;
	}
	frameIdx = 0;
	
	//Clear the first line of all frame buffers.
	//for (int i=0; i<nPtrs; i++) { std::memset(uPtrs[i], '\0', stride); }
	
	//Start acquisition of the desired number of frames
	circ = circ = CiAqStart(sCIp, 0);
	if (kCIEnoErr != circ) {
		show("Err: CiAqStart gave '%s'\n", CiErrStr(circ));
		finalize();
		return;
	}
	
	grabThread = std::thread([this]() { grab(); });
	state = STATE_ACQ;
}


void CDriver_Bitflow::grab() {
	grabbing = true;

	// Check to see if a frame is already available before waiting.
	bool nextFrameReady = false;
	bool CONTINUE = true;
	tCIRC circ;
	while (grabbing & CONTINUE) {
		std::lock_guard<std::mutex> lk(frameMtx);
        nextFrameReady = false;
        while (true) {
    		circ = CiGetOldestNotDeliveredFrame(sCIp, &frameID, &frameP);
    	
    		switch (circ) {
    		case kCIEnoErr: // have the frame
    		    nextFrameReady = true;
    		    break;
    		case kCIEnoNewData: { // wait for another frame.
    			circ = CiWaitNextUndeliveredFrame(sCIp, sTimeoutMS);
    			switch(circ) {
    			case kCIEnoErr:
    				break;
    			case kCIEaqAbortedErr:
    				show("CiWaitNextUndeliveredFrame gave '%s'\n", CiErrStr(circ));
    				finalize();
    				return;
    			default:
    				show("Err: CiWaitNextUndeliveredFrame gave '%s'\n", CiErrStr(circ));
    				finalize();
    				return;
    			}
    			break;
    		}
    		case kCIEaqAbortedErr:
    			show("CiGetOldestNotDeliveredFrame: acqistion aborted\n");
    			finalize();
				return;
    		default:
    			show("CiGetOldestNotDeliveredFrame gave '%s'\n", CiErrStr(circ));
    			finalize();
    			return;
    		}
    		if (nextFrameReady) break;
        }

		switch (state)
		{
			case STATE_ACQ_REC:
				if (sIdx >= sPtrVec.size()) {
					printf("Finish recording\n");
					fflush(stdout);
					state = STATE_ACQ_SAVE;
				} else {
					std::memcpy(sPtrVec[sIdx], frameP, hROIsize * vROIsize * sizeof(unsigned char));
					sIdx++;
				}
			case STATE_ACQ:
				CONTINUE = grabCBFuncPtr(this);
				break;
			case STATE_ACQ_SAVE:
				break;
		}
		frameIdx++;
		// // Get the bufferID for the most recent buffer and release it back
		// // for more DMA.
		// tCIU32 buffID;
		// circ = CiGetBufferID(sCIp, frameID, &buffID);
		// if (kCIEnoErr != circ) {
		// 	show("Err: CiGetBufferID for %d gave '%s'\n",frameID,CiErrStr(circ));
		// 	finalize();
		// }
		// circ = CiReleaseBuffer(sCIp, buffID);
		// if (kCIEnoErr != circ) {
		// 	show("Err: CiRelease for %d gave '%s'\n",buffID,CiErrStr(circ));
		// 	finalize();
		// }
	}

	//Stop acquisition since we are about to free the DMA store
	circ = CiAqAbort(sCIp);
	if (circ != kCIEnoErr) {
		show("Err: CiAqAbort gave '%s'\n", CiErrStr(circ));
	}

	grabbing = false;
}

void CDriver_Bitflow::stop() {
	if  (initialized & ((state == STATE_ACQ) | (state == STATE_ACQ_REC))) {
		if (grabThread.joinable()) {
			grabbing = false;
			grabThread.join();
		}
		state = STATE_IDLE;
	}
}

void CDriver_Bitflow::getImageFormat(unsigned int &width, unsigned int &height, unsigned int &step) { 
	width = hROIsize;
	height = vROIsize;
	step = stride;
}

void CDriver_Bitflow::allocate(int frames) {
	for (auto ptr:sPtrVec) { delete ptr; }

	sPtrVec.clear();
	for (int i=0;i<frames;i++) {
		sPtrVec.push_back(new unsigned char [hROIsize * vROIsize]);
	}
}

void CDriver_Bitflow::saveVideo() {
	state = STATE_ACQ_SAVE;

	time_t CurrentTime = time(NULL);
  	char Now[255];
    strftime(Now, 255, "BITFLOW_%Y%m%d_%H%M%S", localtime(&CurrentTime));

	std::string filename = std::string(Now) + ".avi";

	printf("Save video at %s\n", filename.c_str());
	fflush(stdout);

	cv::VideoWriter ocap;
	ocap.open(filename.c_str(), 0, 30, cv::Size(hROIsize, vROIsize), false);

	if (ocap.isOpened()) {
		for (auto sPtr:sPtrVec) {
			cv::Mat img(vROIsize, hROIsize, CV_8UC1, sPtr, stride);
			ocap << img;
		}
        ocap.release();
		printf("Save finished\n");
		fflush(stdout);
	}
	else {
		printf("Save fail\n");
		fflush(stdout);
	}
	state = STATE_ACQ;
}
