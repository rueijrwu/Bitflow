#pragma once

#include <cstdio>
#include "BFciLib.h"
//#include <opencv2/opencv.hpp>
#include <cstdarg>
#include <string>
#include <atomic>
#include <thread>
#include <memory>
#include <mutex>
#include <iostream>
#include <functional>
#include <vector>
#include <cstring>

class CDriver_Bitflow {

typedef std::atomic<bool> AtomicBool;
typedef std::atomic<int> AtomicInt;

enum STATE{
	STATE_IDLE = 0,
	STATE_ACQ = 1,
	STATE_ACQ_REC = 2,
	STATE_ACQ_SAVE = 3,
};

private:
	int			    sNdx = 0;				    /* device index */
	tCIp			sCIp = nullptr;				/* device open token */
	std::string		sCfgFN= "";					/* for special config file */
	tCIU32			sTimeoutMS = 10000;			/* for catching errors */
	
	tCIU8			**uPtrs = nullptr;
	tCIU32			frameID = 0;
	tCIU8			*frameP = nullptr;
	tCIU32			nFrames = 0;
	tCIU32			nPtrs = 0;
	tCIU32			stride = 0;
	tCIU32 			bitsPerPix;
	tCIU32			hROIoffset = 0;
	tCIU32			hROIsize = 0;
	tCIU32			vROIoffset = 0;
	tCIU32			vROIsize = 0;

	AtomicInt		state;

	AtomicBool		initialized;
	AtomicBool		grabbing;
	
	std::thread		grabThread;
	std::mutex		frameMtx;

	std::function<bool(CDriver_Bitflow*)> grabCBFuncPtr = nullptr;
	
public:
	CDriver_Bitflow(int ndx=0) : sNdx(ndx) {
		initialized = false;
		state = STATE_IDLE;
	}
	virtual ~CDriver_Bitflow() { finalize(); }
	
	void finalize();
	void initialize();
	
	void start();
	void stop();

	void setGrabCallback(std::function<bool(CDriver_Bitflow*)> fPtr) {
		grabCBFuncPtr = fPtr; 
	};

	unsigned long long getFrameIdx() {
		return frameIdx;
	}

	void getImageFormat(unsigned int &width, unsigned int &height, unsigned int &step);
	unsigned char*  getImagePtr() { return frameP; }
	void allocate(int frames);
	void startRecord() { state = STATE_ACQ_REC; sIdx = 0;}
	void saveVideo();
	int getState() { return state; }

private:
	void show(char *format, ...);
	void grab();
	bool configureBuffer();

	unsigned long long frameIdx = 0;
	std::vector<unsigned char*> sPtrVec;
	unsigned int sIdx = 0;
	//unsigned int vIdx = 0;
};