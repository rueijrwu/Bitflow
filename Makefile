CC			= g++
CXXFLAGS	+= -I ./inc -I /usr/include 
CXXFLAGS	+= -std=c++11
LFLAGS 		+= -L ./64b/lib -L /usr/lib/x86_64-linux-gnu
LIB_LIST	= -lpthread
LIB_LIST	+= -lBFciLib -lBFSOciLib.9.06 
LIB_LIST	+= -lopencv_core -lopencv_videoio -lopencv_highgui -lopencv_imgproc -lopencv_imgcodecs

all:	main  

CDriver_Bitflow.o: CDriver_Bitflow.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@ $(LFLAGS) $(LIB_LIST)

test.o: test.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@ $(LFLAGS) $(LIB_LIST)

test: test.o
	$(CC) $(CXXFLAGS) test.o -o $@ $(LFLAGS) $(LIB_LIST)

main.o: main.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@ $(LFLAGS) $(LIB_LIST)

main: main.o CDriver_Bitflow.o
	$(CC) $(CXXFLAGS) main.o CDriver_Bitflow.o -o $@ $(LFLAGS) $(LIB_LIST)

.PHONY: clean
clean:
	@echo "will clean up usr stuff"
	rm -f *.o test main
