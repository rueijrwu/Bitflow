#include "CDriver_Bitflow.hpp"
#include <chrono>
#include <thread>
#include <opencv2/opencv.hpp>
#include <cstring>

enum STATE{
	STATE_ACQ = 1,
	STATE_ACQ_REC = 2,
	STATE_ACQ_SAVE = 3,
};

unsigned int imgWidth = 0;
unsigned int imgHeight = 0;
unsigned int imgSteps = 0;

unsigned char* visPtr = nullptr;

bool newImage = false;

bool grabCallback(CDriver_Bitflow* grabber) {
	unsigned long long frameIdx = grabber->getFrameIdx();
	unsigned char* imgPtr = grabber->getImagePtr();
	if ((frameIdx % 50 == 0) & (imgPtr != nullptr) & (!newImage)) {
		std::memcpy(visPtr, imgPtr, imgWidth * imgHeight * sizeof(unsigned char));
		newImage = true;
		//printf("Frame: %lu\n", grabber->getFrameIdx());
		//fflush(stdout);
	}
	return true;
}

int main() {
	CDriver_Bitflow grabber;
	grabber.setGrabCallback(grabCallback);

	printf("Initializing camera ...\n", imgWidth, imgHeight);
	fflush(stdout);
	grabber.initialize();
	
	grabber.allocate(2000);
	grabber.getImageFormat(imgWidth, imgHeight, imgSteps);
	printf("Start acquiring image (%u x %u)\n", imgWidth, imgHeight);
	fflush(stdout);

	visPtr = new unsigned char[imgWidth * imgHeight];
	cv::namedWindow("Image", cv::WINDOW_NORMAL);
	grabber.start();

	//std::this_thread::sleep_for(std::chrono::milliseconds(3000));
	bool RUN = true;
	while (RUN) {
		switch (grabber.getState()) {
			case STATE_ACQ:
			{
				if (newImage) {
					cv::Mat img(imgHeight, imgWidth, CV_8UC1, visPtr, imgSteps);
					//cv::Mat vis;
					//cv::cvtColor(img, vis, cv::COLOR_GRAY2RGB);
					cv::imshow("Image", img);
					newImage = false;
				}
				int key = cv::waitKey(10);
				// printf("key: %d\n", key);
				// fflush(stdout);
				if (key == 113) RUN = false;
				else if (key == 115) {
					printf("Star recording...\n");
					fflush(stdout);
					grabber.startRecord();
				}
				break;
			}
			case STATE_ACQ_REC:
			{
				if (newImage) {
					cv::Mat img(imgHeight, imgWidth, CV_8UC1, visPtr, imgSteps);
					//cv::Mat vis;
					//cv::cvtColor(img, vis, cv::COLOR_GRAY2RGB);
					cv::imshow("Image", img);
					newImage = false;
				}
				int key = cv::waitKey(10);
				if (key == 113) RUN = false;
                break;
			}
			case STATE_ACQ_SAVE:
				grabber.saveVideo();
				break;
			default:
				break;	
		}
	}
	// //vwriter << img;

	grabber.stop();
	grabber.finalize();
	delete visPtr;
}
