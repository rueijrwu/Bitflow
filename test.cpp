#include <cstdio>
#include "BFciLib.h"
#include <opencv2/opencv.hpp>
#include <cstdarg>

void LDBdisplay(char *format, ...)
{
	va_list	val;
	char	buff[8192];

	va_start(val,format);
	vsprintf(buff,format,val);
	va_end(val);
	printf("LDB: %s\n",buff);
	fflush(stdout);
}

void ERR(char *format, ...)
{
	va_list	val;
	char	buff[8192];
	va_start(val, format);
	vsprintf(buff, format, val);
	va_end(val);
	std::printf("Err: %s", buff);
	fflush(stdout);
}

void SHOW(char *format, ...)
{
	va_list	val;
	char	buff[8192];
	va_start(val, format);
	vsprintf(buff, format, val);
	va_end(val);
	std::printf("%s", buff);
	fflush(stdout);
}

void finalize(tCIp sCIp, tCIU8 **uPtrs=NULL) {
	//Stop acquisition since we are about to free the DMA store
	tCIRC circ = CiAqAbort(sCIp);
	if (circ != kCIEnoErr) {
		ERR("CiAqAbort gave '%s'\n", CiErrStr(circ));
	}
	
	//Unmap the frame buffers.
	if ((NULL != uPtrs) && (kCIEnoErr != CiUnmapFrameBuffers(sCIp))) {
		ERR("CiUnmapFrameBuffers gave '%s'\n",CiErrStr(circ));
	}
	
	//Release the DMA storage.
	circ = CiDrvrBuffConfigure(sCIp, 0, 0, 0, 0, 0);
	if (circ != kCIEnoErr) {
		ERR("CiDrvrBuffConfigure(2) gave '%s'\n",CiErrStr(circ));
	}
	
	//Close the access.
	if ((NULL != sCIp) && (kCIEnoErr != CiVFGclose(sCIp))) {
		ERR("CiVFGclose gave '%s'\n", CiErrStr(circ));
	}
}

int main() {
	int			sNdx = 0;				/* device index */
	tCIp			sCIp = NULL;				/* device open token */
	int			sMaxFrames = 1;				/* total frames to capture */
	int			sDidFrames = 0;				/* total frames handled */
	char			sCfgFN[1024] = "";			/* for special config file */
	tCIU32			sTimeoutMS = 10000;			/* for catching errors */
	
	tCIRC			circ;
	//tCIDOUBLE		a=-1.0, b, c, d;
	// tCIU64			totalBytes=0, totalLines=0;
	tCIU32			nPtrs, counter=sMaxFrames;
	tCIU8			**uPtrs=NULL;
	tCIU32			frameID;
	tCIU8			*frameP;
	tCIU32			nFrames, bitsPerPix, hROIoffset, hROIsize, vROIoffset, vROIsize, stride;
	
	//Set library debug function if '-D' was specified.
	circ = CiSetDebug(NULL, -1, LDBdisplay);
	if (circ != kCIEnoErr) {
		ERR("CiSetDebug of %d gave '%s'\n", sNdx, CiErrStr(circ));
		return 0;
	}
	
	//Open the ndx'th frame grabber with exclusive write permission.
	circ = CiVFGopen(sNdx, kCIBO_exclusiveWrAccess, &sCIp);
	if (circ != kCIEnoErr) {
		ERR("CiVFGopen gave '%s'\n", CiErrStr(circ));
		return 0;
	}
	
	//Init the VFG with the config file specified by argv (or DIP switches)
	circ = CiVFGinitialize(sCIp,sCfgFN);
	if (circ != kCIEnoErr) {
		ERR("CiVFGinitialize gave '%s'\n", CiErrStr(circ));
		finalize(sCIp, uPtrs);
		return 0;
	}
	
	//Configure the VFG for 2 extra frame buffers.
	circ = CiDrvrBuffConfigure(sCIp, sMaxFrames+2, 0, 0, 0, 0);
	if (circ != kCIEnoErr) {
		ERR("CiDrvrBuffConfigure(1) gave '%s'\n", CiErrStr(circ));
		finalize(sCIp, uPtrs);
		return 0;
	}
	
	//Determine buffer configuration.
	circ = CiBufferInterrogate(
			sCIp,
			&nFrames,
			&bitsPerPix,
			&hROIoffset, 
			&hROIsize,
			&vROIoffset,
			&vROIsize,
			&stride);
	if (kCIEnoErr != circ) {
		ERR("CiBufferInterrogate gave '%s'\n", CiErrStr(circ));
		finalize(sCIp, uPtrs);
		return 0;
	}
	
	//Get the buffer pointers for read access to buffers.
	circ = CiMapFrameBuffers(sCIp, 0, &nPtrs, &uPtrs);
	if (kCIEnoErr != circ) {
		ERR("CiMapFrameBuffers gave '%s'\n", CiErrStr(circ));
		finalize(sCIp, uPtrs);
		return 0;
	}
	
	//Reset acquisition and clear all error conditions.
	circ = CiAqSWreset(sCIp);
	if (kCIEnoErr != circ) {
		ERR("CiAqSWreset gave '%s'\n", CiErrStr(circ));
		finalize(sCIp, uPtrs);
		return 0;
	}
	
	
	//cv::VideoWriter vwriter("out.avi");
	cv::namedWindow("Image");
	
	//Start acquisition of the desired number of frames
	circ = CiAqStart(sCIp, sMaxFrames);
	if (kCIEnoErr != circ) {
		ERR("CiAqStart gave '%s'\n", CiErrStr(circ));
		finalize(sCIp, uPtrs);
		return 0;
	}
	
	// Check to see if a frame is already available before waiting.
	for (int i=0;i<sMaxFrames;i++) {

        bool quit = false;
        while (true) {
    		circ = CiGetOldestNotDeliveredFrame(sCIp, &frameID, &frameP);
    	
    		switch (circ) {
    		case kCIEnoErr: // have the frame
    		    quit = true;
    		    break;
    		case kCIEnoNewData: { // wait for another frame.
    			circ = CiWaitNextUndeliveredFrame(sCIp, sTimeoutMS);
    			switch(circ) {
    			case kCIEnoErr:
    				break;
    			case kCIEaqAbortedErr:
    				SHOW("CiWaitNextUndeliveredFrame gave '%s'\n", CiErrStr(circ));
    				finalize(sCIp, uPtrs);
    				return 0;
    			default:
    				ERR("CiWaitNextUndeliveredFrame gave '%s'\n", CiErrStr(circ));
    				finalize(sCIp, uPtrs);
    				return 0;
    			}
    			break;
    		}
    		case kCIEaqAbortedErr:
    			SHOW("CiGetOldestNotDeliveredFrame: acqistion aborted\n");
    			finalize(sCIp, uPtrs);
    			return 0;
    		default:
    			ERR("CiGetOldestNotDeliveredFrame gave '%s'\n", CiErrStr(circ));
    			finalize(sCIp, uPtrs);
    			return 0;
    		}
    		if (quit) break;
        }
		if (frameP) {
			cv::Mat img(vROIsize, hROIsize, CV_8UC1, frameP, stride);
			//cv::Mat vis;
			//cv::cvtColor(img, vis, CV_GRAY2RGB);
			cv::imwrite("test.png", img);
			//cv::imshow("Image", img);
	    	//cv::waitKey(1);
			//vwriter << img;
		}
		
		// totalLines += vROIsize;
		// totalBytes += stride * vROIsize;
		sDidFrames += 1;
	}
	
	finalize(sCIp, uPtrs);
	
	// c = b = GetTime() - a;
	// if ((a < 0.0) || (c < 0.001)) {
		// a = 0.0;
		// b = 0.0;
		// c = 0.0;
		// d = 0.0;
	// } else {
		// a = ((tCIDOUBLE)totalBytes)/c;
		// b = ((tCIDOUBLE)totalLines)/c;
		// d = ((tCIDOUBLE)sDidFrames)/c;
	// }
	
	// SHOW((
		// "%d: Data rate %.1lf ln/s (%.1lf b/s) (%.1lf FPS) (%.1lf sec) after %d fr\n",
		// sNdx, b, a, d, c, sDidFrames));
		
	cv::destroyWindow("Image");
	return 0;
}